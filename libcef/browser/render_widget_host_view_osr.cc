// Copyright (c) 2012 The Chromium Embedded Framework Authors.
// Portions copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "libcef/browser/backing_store_osr.h"
#include "libcef/browser/browser_host_impl.h"
#include "libcef/browser/render_widget_host_view_osr.h"
#include "libcef/common/content_client.h"

#include "base/message_loop/message_loop.h"
#include "content/browser/renderer_host/render_widget_host_impl.h"
#include "content/browser/renderer_host/ui_events_helper.h"
#include "content/public/browser/content_browser_client.h"
#include "content/public/browser/render_view_host.h"
#include "content/browser/renderer_host/ui_events_helper.h"
#include "third_party/WebKit/public/web/WebScreenInfo.h"
#include "webkit/common/cursors/webcursor.h"
#include "ui/base/sequential_id_generator.h"
#include "ui/events/event.h"
#include "ui/events/event_utils.h"
#include "ui/gfx/win/dpi.h"

namespace {

const float kDefaultScaleFactor = 1.0;

static WebKit::WebScreenInfo webScreenInfoFrom(const CefScreenInfo& src) {
  WebKit::WebScreenInfo webScreenInfo;
  webScreenInfo.deviceScaleFactor = src.device_scale_factor;
  webScreenInfo.depth = src.depth;
  webScreenInfo.depthPerComponent = src.depth_per_component;
  webScreenInfo.isMonochrome = src.is_monochrome;
  webScreenInfo.rect = WebKit::WebRect(src.rect.x, src.rect.y,
                                       src.rect.width, src.rect.height);
  webScreenInfo.availableRect = WebKit::WebRect(src.available_rect.x,
                                                src.available_rect.y,
                                                src.available_rect.width,
                                                src.available_rect.height);

  return webScreenInfo;
}

}  // namespace

#if defined(OS_WIN)
// Wrapper for maintaining touchstate associated with a WebTouchEvent.
// Original Chromium code which inspired this: chromium\src\content\browser\renderer_host\render_widget_host_view_win.cc
// Most changes consist in replacing original chromium code which was injecting Window's touchpoint structure (TOUCHINPUT) by our custom CefTouchPoint structure.
class CefWebTouchState {
  public:
    explicit CefWebTouchState(const CefRenderWidgetHostViewOSR* osrWindow);
    
    // Updates the current touchpoint state with the supplied touches.
    size_t UpdateTouchPoints(CefTouchEvent& event, size_t offset);
    
    // Marks all active touchpoints as released.
    bool ReleaseTouchPoints();
    
    // The contained WebTouchEvent.
    const WebKit::WebTouchEvent& touch_event() { return touch_event_; }
    
    // Returns if any touches are modified in the event.
    bool is_changed() { return touch_event_.changedTouchesLength != 0; }
    
  private:
    // Adds a touch point or returns NULL if there's not enough space.
    WebKit::WebTouchPoint* AddTouchPoint(const CefTouchPoint& touch_input);
    
    // Copy details from a TOUCHINPUT to an existing WebTouchPoint, returning
    // true if the resulting point is a stationary move.
    bool UpdateTouchPoint(WebKit::WebTouchPoint* touch_point,
    					  const CefTouchPoint& touch_input);
    
    // Find (or create) a mapping for _os_touch_id_.
    unsigned int GetMappedTouch(unsigned int os_touch_id);
    
    // Remove any mappings that are no longer in use.
    void RemoveExpiredMappings();
    
    WebKit::WebTouchEvent touch_event_;
    const CefRenderWidgetHostViewOSR* const window_;
    
    ui::SequentialIDGenerator id_generator_;
    
    DISALLOW_COPY_AND_ASSIGN(CefWebTouchState);
};
#endif	// defined(OS_WIN)

// Creates a WebGestureEvent corresponding to the given |gesture|
WebKit::WebGestureEvent CreateWebGestureEvent(const ui::GestureEvent& gesture) {
  WebKit::WebGestureEvent gesture_event = content::MakeWebGestureEventFromUIEvent(gesture);

  // We cannot know globalX and globalY in OSR mode...
  gesture_event.x = gesture.location().x();
  gesture_event.y = gesture.location().y();
  gesture_event.globalX = gesture.location().x();
  gesture_event.globalY = gesture.location().y();

  return gesture_event;
}

WebKit::WebGestureEvent CreateFlingCancelEvent(double time_stamp) {
  WebKit::WebGestureEvent gesture_event;
  gesture_event.timeStampSeconds = time_stamp;
  gesture_event.type = WebKit::WebGestureEvent::GestureFlingCancel;
  gesture_event.sourceDevice = WebKit::WebGestureEvent::Touchscreen;
  return gesture_event;
}

ui::EventType ConvertToUIEvent(WebKit::WebTouchPoint::State t) {
  switch (t) {
    case WebKit::WebTouchPoint::StatePressed:
      return ui::ET_TOUCH_PRESSED;
    case WebKit::WebTouchPoint::StateMoved:
      return ui::ET_TOUCH_MOVED;
    case WebKit::WebTouchPoint::StateStationary:
      return ui::ET_TOUCH_STATIONARY;
    case WebKit::WebTouchPoint::StateReleased:
      return ui::ET_TOUCH_RELEASED;
    case WebKit::WebTouchPoint::StateCancelled:
      return ui::ET_TOUCH_CANCELLED;
    default:
      DCHECK(false) << "Unexpected ui type. " << t;
      return ui::ET_UNKNOWN;
  }
}

class TouchEventFromWebTouchPoint : public ui::TouchEvent {
 public:
  TouchEventFromWebTouchPoint(const WebKit::WebTouchPoint& touch_point,
                              base::TimeDelta& timestamp)
      : ui::TouchEvent(ConvertToUIEvent(touch_point.state),
                       touch_point.position,
                       touch_point.id,
                       timestamp) {
    set_radius(touch_point.radiusX, touch_point.radiusY);
    set_rotation_angle(touch_point.rotationAngle);
    set_force(touch_point.force);
    set_flags(ui::GetModifiersFromKeyState());
  }

  virtual ~TouchEventFromWebTouchPoint() {}

 private:
  DISALLOW_COPY_AND_ASSIGN(TouchEventFromWebTouchPoint);
};

///////////////////////////////////////////////////////////////////////////////
// CefRenderWidgetHostViewOSR, public:

CefRenderWidgetHostViewOSR::CefRenderWidgetHostViewOSR(
    content::RenderWidgetHost* widget)
        : weak_factory_(this),
          render_widget_host_(content::RenderWidgetHostImpl::From(widget)),
          parent_host_view_(NULL),
          popup_host_view_(NULL),
          about_to_validate_and_paint_(false)
#if defined(OS_MACOSX)
          , text_input_context_osr_mac_(NULL)
#endif
#if defined(OS_WIN)
          , touch_state_(new CefWebTouchState(this))
          , gesture_recognizer_(ui::GestureRecognizer::Create(this))
#endif	// defined(OS_WIN)
          {
  DCHECK(render_widget_host_);
  render_widget_host_->SetView(this);

  // CefBrowserHostImpl might not be created at this time for popups.
  if (render_widget_host_->IsRenderView()) {
    browser_impl_ = CefBrowserHostImpl::GetBrowserForHost(
        content::RenderViewHost::From(render_widget_host_));
  }
}

CefRenderWidgetHostViewOSR::~CefRenderWidgetHostViewOSR() {
}


// RenderWidgetHostView implementation.
void CefRenderWidgetHostViewOSR::InitAsChild(gfx::NativeView parent_view) {
}

content::RenderWidgetHost*
    CefRenderWidgetHostViewOSR::GetRenderWidgetHost() const {
  return render_widget_host_;
}

void CefRenderWidgetHostViewOSR::SetSize(const gfx::Size& size) {
}

void CefRenderWidgetHostViewOSR::SetBounds(const gfx::Rect& rect) {
}

gfx::NativeView CefRenderWidgetHostViewOSR::GetNativeView() const {
  return NULL;
}

gfx::NativeViewId CefRenderWidgetHostViewOSR::GetNativeViewId() const {
  if (!browser_impl_.get())
    return gfx::NativeViewId();
  // This Id is used on Windows as HWND to retrieve monitor info
  // If this Id is not a valid window, the main screen monitor info is used
  return reinterpret_cast<gfx::NativeViewId>(browser_impl_->GetWindowHandle());
}

gfx::NativeViewAccessible
    CefRenderWidgetHostViewOSR::GetNativeViewAccessible() {
  return gfx::NativeViewAccessible();
}

bool CefRenderWidgetHostViewOSR::HasFocus() const {
  return false;
}

bool CefRenderWidgetHostViewOSR::IsSurfaceAvailableForCopy() const {
  return false;
}

void CefRenderWidgetHostViewOSR::Show() {
  WasShown();
}

void CefRenderWidgetHostViewOSR::Hide() {
  WasHidden();
}

bool CefRenderWidgetHostViewOSR::IsShowing() {
  return true;
}

gfx::Rect CefRenderWidgetHostViewOSR::GetViewBounds() const {
  if (IsPopupWidget())
    return popup_position_;

  if (!browser_impl_.get())
    return gfx::Rect();
  CefRect rc;
  browser_impl_->GetClient()->GetRenderHandler()->GetViewRect(
      browser_impl_->GetBrowser(), rc);
  return gfx::Rect(rc.x, rc.y, rc.width, rc.height);
}

// Implementation of RenderWidgetHostViewPort.
void CefRenderWidgetHostViewOSR::InitAsPopup(
    RenderWidgetHostView* parent_host_view,
    const gfx::Rect& pos) {
  parent_host_view_ = static_cast<CefRenderWidgetHostViewOSR*>(
      parent_host_view);
  browser_impl_ = parent_host_view_->get_browser_impl();
  if (!browser_impl_.get())
    return;

  parent_host_view_->CancelWidget();

  parent_host_view_->set_popup_host_view(this);
  NotifyShowWidget();

  popup_position_ = pos;
  NotifySizeWidget();
}

void CefRenderWidgetHostViewOSR::InitAsFullscreen(
    RenderWidgetHostView* reference_host_view) {
  NOTREACHED() << "Fullscreen widgets are not supported in OSR";
}

void CefRenderWidgetHostViewOSR::WasShown() {
  if (render_widget_host_)
    render_widget_host_->WasShown();
}

void CefRenderWidgetHostViewOSR::WasHidden() {
  if (render_widget_host_)
    render_widget_host_->WasHidden();
}

void CefRenderWidgetHostViewOSR::MovePluginWindows(
    const gfx::Vector2d& scroll_offset,
    const std::vector<content::WebPluginGeometry>& moves) {
}

void CefRenderWidgetHostViewOSR::Focus() {
}

void CefRenderWidgetHostViewOSR::Blur() {
}

void CefRenderWidgetHostViewOSR::UpdateCursor(const WebCursor& cursor) {
  TRACE_EVENT0("libcef", "CefRenderWidgetHostViewOSR::UpdateCursor");
  if (!browser_impl_.get())
    return;
#if defined(OS_WIN)
  HMODULE hModule = ::GetModuleHandle(
      CefContentClient::Get()->browser()->GetResourceDllName());
  if (!hModule)
    hModule = ::GetModuleHandle(NULL);
  WebCursor web_cursor = cursor;
  HCURSOR hCursor = web_cursor.GetCursor((HINSTANCE)hModule);
  browser_impl_->GetClient()->GetRenderHandler()->OnCursorChange(
      browser_impl_->GetBrowser(), hCursor);
#elif defined(OS_MACOSX) || defined(TOOLKIT_GTK)
  // cursor is const, and GetNativeCursor is not
  WebCursor web_cursor = cursor;
  CefCursorHandle native_cursor = web_cursor.GetNativeCursor();
  browser_impl_->GetClient()->GetRenderHandler()->OnCursorChange(
      browser_impl_->GetBrowser(), native_cursor);
#else
  // TODO(port): Implement this method to work on other platforms as part of
  // off-screen rendering support.
  NOTREACHED();
#endif
}

void CefRenderWidgetHostViewOSR::SetIsLoading(bool is_loading) {
}

#if !defined(OS_MACOSX)
void CefRenderWidgetHostViewOSR::TextInputTypeChanged(
    ui::TextInputType type,
    ui::TextInputMode mode,
    bool can_compose_inline) {
}

void CefRenderWidgetHostViewOSR::ImeCancelComposition() {
}

#if defined(OS_WIN) || defined(USE_AURA)
void CefRenderWidgetHostViewOSR::ImeCompositionRangeChanged(
    const gfx::Range& range,
    const std::vector<gfx::Rect>& character_bounds) {
}
#endif
#endif  // !defined(OS_MACOSX)

void CefRenderWidgetHostViewOSR::DidUpdateBackingStore(
    const gfx::Rect& scroll_rect,
    const gfx::Vector2d& scroll_delta,
    const std::vector<gfx::Rect>& copy_rects,
    const ui::LatencyInfo& latency_info) {
  if (!scroll_rect.IsEmpty()) {
    std::vector<gfx::Rect> dirty_rects(copy_rects);
    dirty_rects.push_back(scroll_rect);
    Paint(dirty_rects);
  } else {
    Paint(copy_rects);
  }
}

void CefRenderWidgetHostViewOSR::RenderProcessGone(
    base::TerminationStatus status,
    int error_code) {
  render_widget_host_ = NULL;
  parent_host_view_ = NULL;
  popup_host_view_ = NULL;
}

#if defined(OS_WIN) && !defined(USE_AURA)
void CefRenderWidgetHostViewOSR::WillWmDestroy() {
  // Will not be called if GetNativeView returns NULL.
  NOTREACHED();
}
#endif

void CefRenderWidgetHostViewOSR::GetScreenInfo(WebKit::WebScreenInfo* results) {
  if (!browser_impl_.get())
    return;

  CefScreenInfo screen_info(
      kDefaultScaleFactor, 0, 0, false, CefRect(), CefRect());

  CefRefPtr<CefRenderHandler> handler =
      browser_impl_->client()->GetRenderHandler();
  if (!handler->GetScreenInfo(browser_impl_.get(), screen_info) ||
      screen_info.rect.width == 0 ||
      screen_info.rect.height == 0 ||
      screen_info.available_rect.width == 0 ||
      screen_info.available_rect.height == 0) {
    // If a screen rectangle was not provided, try using the view rectangle
    // instead. Otherwise, popup views may be drawn incorrectly, or not at all.
    CefRect screenRect;
    if (!handler->GetViewRect(browser_impl_.get(), screenRect)) {
      NOTREACHED();
      screenRect = CefRect();
    }

    if (screen_info.rect.width == 0 && screen_info.rect.height == 0)
      screen_info.rect = screenRect;

    if (screen_info.available_rect.width == 0 &&
        screen_info.available_rect.height == 0)
      screen_info.available_rect = screenRect;
  }

  *results = webScreenInfoFrom(screen_info);
}

gfx::Rect CefRenderWidgetHostViewOSR::GetBoundsInRootWindow() {
  if (!browser_impl_.get())
    return gfx::Rect();
  CefRect rc;
  if (browser_impl_->GetClient()->GetRenderHandler()->GetRootScreenRect(
          browser_impl_->GetBrowser(), rc)) {
    return gfx::Rect(rc.x, rc.y, rc.width, rc.height);
  }
  return gfx::Rect();
}

void CefRenderWidgetHostViewOSR::OnAccessibilityEvents(
    const std::vector<AccessibilityHostMsg_EventParams>& params) {
}

void CefRenderWidgetHostViewOSR::Destroy() {
  if (IsPopupWidget()) {
    if (parent_host_view_)
      parent_host_view_->CancelWidget();
  } else {
    CancelWidget();
  }

  delete this;
}

void CefRenderWidgetHostViewOSR::SetTooltipText(const string16& tooltip_text) {
  if (!browser_impl_.get())
    return;

  CefString tooltip(tooltip_text);
  CefRefPtr<CefDisplayHandler> handler =
      browser_impl_->GetClient()->GetDisplayHandler();
  if (handler.get()) {
    handler->OnTooltip(browser_impl_->GetBrowser(), tooltip);
  }
}

void CefRenderWidgetHostViewOSR::SelectionBoundsChanged(
    const ViewHostMsg_SelectionBounds_Params& params) {
}

void CefRenderWidgetHostViewOSR::ScrollOffsetChanged() {
  if (!browser_impl_.get())
    return;
  browser_impl_->GetClient()->GetRenderHandler()->
      OnScrollOffsetChanged(browser_impl_.get());
}

content::BackingStore* CefRenderWidgetHostViewOSR::AllocBackingStore(
    const gfx::Size& size) {
  return render_widget_host_ ?
      new BackingStoreOSR(render_widget_host_, size, GetDeviceScaleFactor()) :
      NULL;
}

void CefRenderWidgetHostViewOSR::CopyFromCompositingSurface(
    const gfx::Rect& src_subrect,
    const gfx::Size& dst_size,
    const base::Callback<void(bool, const SkBitmap&)>& callback) {
}

void CefRenderWidgetHostViewOSR::CopyFromCompositingSurfaceToVideoFrame(
    const gfx::Rect& src_subrect,
    const scoped_refptr<media::VideoFrame>& target,
    const base::Callback<void(bool)>& callback) {
}

bool CefRenderWidgetHostViewOSR::CanCopyToVideoFrame() const {
  return false;
}

void CefRenderWidgetHostViewOSR::OnAcceleratedCompositingStateChange() {
}

void CefRenderWidgetHostViewOSR::SetHasHorizontalScrollbar(
    bool has_horizontal_scrollbar) {
}

void CefRenderWidgetHostViewOSR::SetScrollOffsetPinning(
    bool is_pinned_to_left, bool is_pinned_to_right) {
}

gfx::GLSurfaceHandle CefRenderWidgetHostViewOSR::GetCompositingSurface() {
  return gfx::GLSurfaceHandle();
}

void CefRenderWidgetHostViewOSR::AcceleratedSurfaceBuffersSwapped(
    const GpuHostMsg_AcceleratedSurfaceBuffersSwapped_Params& params,
    int gpu_host_id) {
}

void CefRenderWidgetHostViewOSR::AcceleratedSurfacePostSubBuffer(
    const GpuHostMsg_AcceleratedSurfacePostSubBuffer_Params& params,
    int gpu_host_id) {
}

void CefRenderWidgetHostViewOSR::AcceleratedSurfaceSuspend() {
}

void CefRenderWidgetHostViewOSR::AcceleratedSurfaceRelease() {
}

bool CefRenderWidgetHostViewOSR::HasAcceleratedSurface(
    const gfx::Size& desired_size) {
  return false;
}

bool CefRenderWidgetHostViewOSR::LockMouse() {
  return false;
}

void CefRenderWidgetHostViewOSR::UnlockMouse() {
}

#if defined(OS_WIN) && !defined(USE_AURA)
void CefRenderWidgetHostViewOSR::SetClickthroughRegion(SkRegion* region) {
}
#endif

void CefRenderWidgetHostViewOSR::SetBackground(const SkBitmap& background) {
  if (!render_widget_host_)
    return;
  RenderWidgetHostViewBase::SetBackground(background);
  render_widget_host_->SetBackground(background);
}

void CefRenderWidgetHostViewOSR::Invalidate(const gfx::Rect& rect,
    CefBrowserHost::PaintElementType type) {
  TRACE_EVENT1("libcef", "CefRenderWidgetHostViewOSR::Invalidate", "type", type); 
  if (!IsPopupWidget() && type == PET_POPUP) {
    if (popup_host_view_)
      popup_host_view_->Invalidate(rect, type);
    return;
  }
  std::vector<gfx::Rect> dirtyRects;
  dirtyRects.push_back(rect);
  Paint(dirtyRects);
}

void CefRenderWidgetHostViewOSR::Paint(
    const std::vector<gfx::Rect>& copy_rects) {
  TRACE_EVENT1("libcef", "CefRenderWidgetHostViewOSR::Paint", "rects", copy_rects.size()); 
  if (about_to_validate_and_paint_ ||
      !browser_impl_.get() ||
      !render_widget_host_) {
    pending_update_rects_.insert(pending_update_rects_.end(),
        copy_rects.begin(), copy_rects.end());
    return;
  }

  about_to_validate_and_paint_ = true;
  BackingStoreOSR* backing_store =
      BackingStoreOSR::From(render_widget_host_->GetBackingStore(true));
  about_to_validate_and_paint_ = false;

  if (backing_store) {
    const gfx::Rect client_rect(backing_store->size());
    SkRegion damaged_rgn;

    for (size_t i = 0; i < copy_rects.size(); ++i) {
      SkIRect skRect = SkIRect::MakeLTRB(
          copy_rects[i].x(), copy_rects[i].y(),
          copy_rects[i].right(), copy_rects[i].bottom());
      damaged_rgn.op(skRect, SkRegion::kUnion_Op);
    }

    for (size_t i = 0; i < pending_update_rects_.size(); ++i) {
      SkIRect skRect = SkIRect::MakeLTRB(
          pending_update_rects_[i].x(), pending_update_rects_[i].y(),
          pending_update_rects_[i].right(), pending_update_rects_[i].bottom());
      damaged_rgn.op(skRect, SkRegion::kUnion_Op);
    }
    pending_update_rects_.clear();

    CefRenderHandler::RectList rcList;
    SkRegion::Cliperator iterator(damaged_rgn,
        SkIRect::MakeWH(client_rect.width(), client_rect.height()));
    for (; !iterator.done(); iterator.next()) {
      const SkIRect& r = iterator.rect();
      rcList.push_back(
          CefRect(r.left(), r.top(), r.width(), r.height()));
    }

    if (rcList.size() == 0)
      return;

    browser_impl_->GetClient()->GetRenderHandler()->OnPaint(
        browser_impl_->GetBrowser(),
        IsPopupWidget() ? PET_POPUP : PET_VIEW,
        rcList,
        backing_store->getPixels(),
        client_rect.width(),
        client_rect.height());
  }
}

bool CefRenderWidgetHostViewOSR::InstallTransparency() {
  if (browser_impl_.get() && browser_impl_->IsTransparent()) {
    SkBitmap bg;
    bg.setConfig(SkBitmap::kARGB_8888_Config, 1, 1);  // 1x1 alpha bitmap.
    bg.allocPixels();
    bg.eraseARGB(0x00, 0x00, 0x00, 0x00);
    SetBackground(bg);
    return true;
  }
  return false;
}

void CefRenderWidgetHostViewOSR::CancelWidget() {
  if (IsPopupWidget()) {
    if (render_widget_host_)
      render_widget_host_->LostCapture();

    if (browser_impl_.get()) {
      NotifyHideWidget();
      browser_impl_ = NULL;
    }

    if (parent_host_view_) {
      parent_host_view_->set_popup_host_view(NULL);
      parent_host_view_ = NULL;
    }

    if (!weak_factory_.HasWeakPtrs()) {
      base::MessageLoop::current()->PostTask(FROM_HERE,
          base::Bind(&CefRenderWidgetHostViewOSR::ShutdownHost,
          weak_factory_.GetWeakPtr()));
    }
  } else if (popup_host_view_) {
    popup_host_view_->CancelWidget();
  }
}

void CefRenderWidgetHostViewOSR::NotifyShowWidget() {
  if (browser_impl_.get()) {
    browser_impl_->GetClient()->GetRenderHandler()->OnPopupShow(
        browser_impl_->GetBrowser(), true);
  }
}

void CefRenderWidgetHostViewOSR::NotifyHideWidget() {
  if (browser_impl_.get()) {
    browser_impl_->GetClient()->GetRenderHandler()->OnPopupShow(
        browser_impl_->GetBrowser(), false);
  }
}

void CefRenderWidgetHostViewOSR::NotifySizeWidget() {
  if (browser_impl_.get()) {
    CefRect widget_pos(popup_position_.x(), popup_position_.y(),
                       popup_position_.width(), popup_position_.height());
    browser_impl_->GetClient()->GetRenderHandler()->OnPopupSize(
        browser_impl_->GetBrowser(), widget_pos);
  }
}

CefRefPtr<CefBrowserHostImpl>
    CefRenderWidgetHostViewOSR::get_browser_impl() const {
  return browser_impl_;
}

void CefRenderWidgetHostViewOSR::set_browser_impl(
    CefRefPtr<CefBrowserHostImpl> browser) {
  browser_impl_ = browser;
}

void CefRenderWidgetHostViewOSR::set_popup_host_view(
    CefRenderWidgetHostViewOSR* popup_view) {
  popup_host_view_ = popup_view;
}

void CefRenderWidgetHostViewOSR::ShutdownHost() {
  weak_factory_.InvalidateWeakPtrs();
  if (render_widget_host_)
    render_widget_host_->Shutdown();
  // Do not touch any members at this point, |this| has been deleted.
}

void CefRenderWidgetHostViewOSR::set_parent_host_view(
    CefRenderWidgetHostViewOSR* parent_view) {
  parent_host_view_ = parent_view;
}

void CefRenderWidgetHostViewOSR::SendKeyEvent(
    const content::NativeWebKeyboardEvent& event) {
  TRACE_EVENT0("libcef", "CefRenderWidgetHostViewOSR::SendKeyEvent");
  if (!render_widget_host_)
    return;
  render_widget_host_->ForwardKeyboardEvent(event);
}

void CefRenderWidgetHostViewOSR::SendMouseEvent(
    const WebKit::WebMouseEvent& event) {
  TRACE_EVENT0("libcef", "CefRenderWidgetHostViewOSR::SendMouseEvent");
  if (!IsPopupWidget() && popup_host_view_) {
    if (popup_host_view_->popup_position_.Contains(event.x, event.y)) {
      WebKit::WebMouseEvent popup_event(event);
      popup_event.x -= popup_host_view_->popup_position_.x();
      popup_event.y -= popup_host_view_->popup_position_.y();
      popup_event.windowX = popup_event.x;
      popup_event.windowY = popup_event.y;

      popup_host_view_->SendMouseEvent(popup_event);
      return;
    }
  }
  if (!render_widget_host_)
    return;
  render_widget_host_->ForwardMouseEvent(event);
}

void CefRenderWidgetHostViewOSR::SendMouseWheelEvent(
    const WebKit::WebMouseWheelEvent& event) {
  TRACE_EVENT0("libcef", "CefRenderWidgetHostViewOSR::SendMouseWheelEvent");
  if (!IsPopupWidget() && popup_host_view_) {
    if (popup_host_view_->popup_position_.Contains(event.x, event.y)) {
      WebKit::WebMouseWheelEvent popup_event(event);
      popup_event.x -= popup_host_view_->popup_position_.x();
      popup_event.y -= popup_host_view_->popup_position_.y();
      popup_event.windowX = popup_event.x;
      popup_event.windowY = popup_event.y;
      popup_host_view_->SendMouseWheelEvent(popup_event);
      return;
    } else {
      // scrolling outside the popup widget, will destroy widget
      CancelWidget();
    }
  }
  if (!render_widget_host_)
    return;
  render_widget_host_->ForwardWheelEvent(event);
}

#if defined(OS_WIN)
void CefRenderWidgetHostViewOSR::SendTouchEvent(const CefTouchEvent& event) {
  // Copy event because we van modify it internally
  CefTouchEvent touchEvent = event;

  size_t total = touchEvent.count;
  for (size_t start = 0; start < total;) {
    start += touch_state_->UpdateTouchPoints(touchEvent, start);
    if(render_widget_host_->ShouldForwardTouchEvent()) {
      if (touch_state_->is_changed())
        render_widget_host_->ForwardTouchEventWithLatencyInfo(touch_state_->touch_event(), ui::LatencyInfo());
    } else {
      // Handle windows scroll and such events.
      const WebKit::WebTouchEvent& touch_event = touch_state_->touch_event();
      base::TimeDelta timestamp = base::TimeDelta::FromMilliseconds(touch_event.timeStampSeconds * 1000);
      for (size_t i = 0; i < touch_event.touchesLength; ++i) {
        scoped_ptr<ui::GestureRecognizer::Gestures> gestures;
        gestures.reset(gesture_recognizer_->ProcessTouchEventForGesture(TouchEventFromWebTouchPoint(touch_event.touches[i], timestamp),
                                                                        ui::ER_UNHANDLED, this));
        ProcessGestures(gestures.get());
      }
    }
  }
}

void CefRenderWidgetHostViewOSR::ProcessGestures(ui::GestureRecognizer::Gestures* gestures) {
  if ((gestures == NULL) || gestures->empty())
    return;

  for (ui::GestureRecognizer::Gestures::iterator g_it = gestures->begin(); g_it != gestures->end(); ++g_it)
    ForwardGestureEventToRenderer(*g_it);
}
#endif	// defined(OS_WIN)

void CefRenderWidgetHostViewOSR::ProcessAckedTouchEvent(const content::TouchEventWithLatencyInfo& touch, content::InputEventAckState ack_result) {
  ScopedVector<ui::TouchEvent> events;
  if (!MakeUITouchEventsFromWebTouchEvents(touch, &events, content::LOCAL_COORDINATES))
    return;

  ui::EventResult result = (ack_result == content::INPUT_EVENT_ACK_STATE_CONSUMED) ? ui::ER_HANDLED : ui::ER_UNHANDLED;
  for (ScopedVector<ui::TouchEvent>::iterator iter = events.begin(), end = events.end(); iter != end; ++iter) {
    scoped_ptr<ui::GestureRecognizer::Gestures> gestures;
    gestures.reset(gesture_recognizer_->ProcessTouchEventForGesture(*(*iter), result, this));
    ProcessGestures(gestures.get());
  }
}

bool CefRenderWidgetHostViewOSR::DispatchLongPressGestureEvent(ui::GestureEvent* event) {
  return ForwardGestureEventToRenderer(event);
}

bool CefRenderWidgetHostViewOSR::DispatchCancelTouchEvent(ui::TouchEvent* event) {
  if (!render_widget_host_ || !render_widget_host_->ShouldForwardTouchEvent())
    return false;
  
  DCHECK(event->type() == WebKit::WebInputEvent::TouchCancel);
  WebKit::WebTouchEvent cancel_event;
  cancel_event.type = WebKit::WebInputEvent::TouchCancel;
  cancel_event.timeStampSeconds = event->time_stamp().InSecondsF();
  render_widget_host_->ForwardTouchEventWithLatencyInfo(cancel_event, *event->latency());
  return true;
}

bool CefRenderWidgetHostViewOSR::ForwardGestureEventToRenderer(ui::GestureEvent* gesture) {
  if (!render_widget_host_)
    return false;

  // Pinch gestures are disabled by default on windows desktop. See
  // crbug.com/128477 and crbug.com/148816
  if (gesture->type() == ui::ET_GESTURE_PINCH_BEGIN ||
      gesture->type() == ui::ET_GESTURE_PINCH_UPDATE ||
      gesture->type() == ui::ET_GESTURE_PINCH_END) {
    return true;
  }

  WebKit::WebGestureEvent web_gesture = CreateWebGestureEvent(*gesture);
  if (web_gesture.type == WebKit::WebGestureEvent::Undefined)
    return false;

  if (web_gesture.type == WebKit::WebGestureEvent::GestureTapDown)
    render_widget_host_->ForwardGestureEvent(CreateFlingCancelEvent(gesture->time_stamp().InSecondsF()));

  render_widget_host_->ForwardGestureEventWithLatencyInfo(web_gesture, *gesture->latency());
  return true;
}

void CefRenderWidgetHostViewOSR::OnScreenInfoChanged() {
  TRACE_EVENT0("libcef", "CefRenderWidgetHostViewOSR::OnScreenInfoChanged");
  if (!render_widget_host_)
    return;

  BackingStoreOSR* backing_store =
      BackingStoreOSR::From(render_widget_host_->GetBackingStore(true));
  if (backing_store)
    backing_store->ScaleFactorChanged(GetDeviceScaleFactor());

  // What could be taken from UpdateScreenInfo(window_) - updates the renderer
  // cached rectangles
  //render_widget_host_->SendScreenRects();

  render_widget_host_->NotifyScreenInfoChanged();
  // We might want to change the cursor scale factor here as well - see the
  // cache for the current_cursor_, as passed by UpdateCursor from the renderer
  // in the rwhv_aura (current_cursor_.SetScaleFactor)
}

float CefRenderWidgetHostViewOSR::GetDeviceScaleFactor() {
  if (!browser_impl_.get())
    return kDefaultScaleFactor;

  CefScreenInfo screen_info(
      kDefaultScaleFactor, 0, 0, false, CefRect(), CefRect());
  if (!browser_impl_->GetClient()->GetRenderHandler()->GetScreenInfo(
          browser_impl_->GetBrowser(), screen_info)) {
    // Use the default
    return kDefaultScaleFactor;
  }

  return screen_info.device_scale_factor;
}

#if defined(OS_MACOSX)
void CefRenderWidgetHostViewOSR::AboutToWaitForBackingStoreMsg() {
}

bool CefRenderWidgetHostViewOSR::PostProcessEventForPluginIme(
    const content::NativeWebKeyboardEvent& event) {
  return false;
}
#endif

#if defined(OS_MACOSX)
void CefRenderWidgetHostViewOSR::SetActive(bool active) {
}

void CefRenderWidgetHostViewOSR::SetTakesFocusOnlyOnMouseDown(bool flag) {
}

void CefRenderWidgetHostViewOSR::SetWindowVisibility(bool visible) {
}

void CefRenderWidgetHostViewOSR::WindowFrameChanged() {
}

void CefRenderWidgetHostViewOSR::ShowDefinitionForSelection() {
}


bool CefRenderWidgetHostViewOSR::SupportsSpeech() const {
  return false;
}

void CefRenderWidgetHostViewOSR::SpeakSelection() {
}

bool CefRenderWidgetHostViewOSR::IsSpeaking() const {
  return false;
}

void CefRenderWidgetHostViewOSR::StopSpeaking() {
}
#endif  // defined(OS_MACOSX)

#if defined(TOOLKIT_GTK)
GdkEventButton* CefRenderWidgetHostViewOSR::GetLastMouseDown() {
  return NULL;
}

gfx::NativeView CefRenderWidgetHostViewOSR::BuildInputMethodsGtkMenu() {
  return NULL;
}
#endif  // defined(TOOLKIT_GTK)

#if defined(OS_WIN)
CefWebTouchState::CefWebTouchState(const CefRenderWidgetHostViewOSR* window)
    : window_(window),
      id_generator_(0) {
}

size_t CefWebTouchState::UpdateTouchPoints(CefTouchEvent& event, size_t offset)
{
  size_t count = event.count - offset;

  // First we reset all touch event state. This involves removing any released
  // touchpoints and marking the rest as stationary. After that we go through
  // and alter/add any touchpoints (from the touch input buffer) that we can
  // coalesce into a single message. The return value is the number of consumed
  // input message.
  WebKit::WebTouchPoint* point = touch_event_.touches;
  WebKit::WebTouchPoint* end = point + touch_event_.touchesLength;
  while (point < end) {
    if (point->state == WebKit::WebTouchPoint::StateReleased) {
      *point = *(--end);
      --touch_event_.touchesLength;
    } else {
      point->state = WebKit::WebTouchPoint::StateStationary;
      point++;
    }
  }

  touch_event_.changedTouchesLength = 0;
  touch_event_.modifiers = content::EventFlagsToWebEventModifiers(ui::GetModifiersFromKeyState());

  // Consume all events of the same type and add them to the changed list.
  int last_type = 0;
  for (size_t i = 0; i < count; ++i) {
    unsigned int mapped_id = GetMappedTouch(event.points[i + offset].id);
    WebKit::WebTouchPoint* point = NULL;
    for (unsigned j = 0; j < touch_event_.touchesLength; ++j) {
      if (static_cast<DWORD>(touch_event_.touches[j].id) == mapped_id) {
        point =  &touch_event_.touches[j];
        break;
      }
    }

    // Use a move instead if we see a down on a point we already have
    // (case of touch pressed inside the window but released outside).
    int type = event.points[i + offset].type;
    if (point && type == TPT_PRESSED) {
      type = TPT_MOVED;
      event.points[i + offset].type = TPT_MOVED;
    } 

    // Stop processing when the event type changes.
    if (touch_event_.changedTouchesLength && type != last_type)
      return i;

    touch_event_.timeStampSeconds = event.points[i + offset].timestamp;

    last_type = type;
    switch (type)
    {
	  case TPT_PRESSED:
      {
        if (!(point = AddTouchPoint(event.points[i + offset])))
          continue;

        touch_event_.type = WebKit::WebInputEvent::TouchStart;
        break;
      }

      case TPT_RELEASED:
      {
        if (!point)  // Just throw away a stray up.
          continue;
        
        point->state = WebKit::WebTouchPoint::StateReleased;
        UpdateTouchPoint(point, event.points[i + offset]);
        touch_event_.type = WebKit::WebInputEvent::TouchEnd;
        break;
      }

      case TPT_MOVED:
      {
        if (point) {
          point->state = WebKit::WebTouchPoint::StateMoved;

          // Don't update the message if the point didn't really move.
          if (UpdateTouchPoint(point, event.points[i + offset]))
            continue;

          touch_event_.type = WebKit::WebInputEvent::TouchMove;
        } else if (touch_event_.changedTouchesLength) {
          RemoveExpiredMappings();

          // Can't add a point if we're already handling move events.
          return i;
        } else {
          // Treat a move with no existing point as a down.
          if (!(point = AddTouchPoint(event.points[i + offset])))
            continue;

          last_type = TPT_RELEASED;
          event.points[i + offset].type = TPT_RELEASED;
          touch_event_.type = WebKit::WebInputEvent::TouchStart;
        }
        break;
      }

      default:
        NOTREACHED();
        continue;
    }
    touch_event_.changedTouches[touch_event_.changedTouchesLength++] = *point;
  }

  RemoveExpiredMappings();
  return count;
}

void CefWebTouchState::RemoveExpiredMappings() {
  WebKit::WebTouchPoint* point = touch_event_.touches;
  WebKit::WebTouchPoint* end = point + touch_event_.touchesLength;
  for (; point < end; ++point) {
    if (point->state == WebKit::WebTouchPoint::StateReleased)
      id_generator_.ReleaseGeneratedID(point->id);
  }
}


bool CefWebTouchState::ReleaseTouchPoints() {
  if (touch_event_.touchesLength == 0)
    return false;

  // Mark every active touchpoint as released.
  touch_event_.type = WebKit::WebInputEvent::TouchEnd;
  touch_event_.changedTouchesLength = touch_event_.touchesLength;
  for (unsigned int i = 0; i < touch_event_.touchesLength; ++i) {
    touch_event_.touches[i].state = WebKit::WebTouchPoint::StateReleased;
    touch_event_.changedTouches[i].state =
      WebKit::WebTouchPoint::StateReleased;
  }

  return true;
}

WebKit::WebTouchPoint* CefWebTouchState::AddTouchPoint(const CefTouchPoint& touch_input) {
  DCHECK(touch_event_.touchesLength < WebKit::WebTouchEvent::touchesLengthCap);
  if (touch_event_.touchesLength >= WebKit::WebTouchEvent::touchesLengthCap)
    return NULL;

  WebKit::WebTouchPoint* point = &touch_event_.touches[touch_event_.touchesLength++];
  point->state = WebKit::WebTouchPoint::StatePressed;
  point->id = GetMappedTouch(touch_input.id);
  UpdateTouchPoint(point, touch_input);
  return point;
}

bool CefWebTouchState::UpdateTouchPoint(WebKit::WebTouchPoint* touch_point,
                                        const CefTouchPoint& touch_input) {
  int coordinatesX = touch_input.x / gfx::win::GetUndocumentedDPITouchScale();
  int coordinatesY = touch_input.y / gfx::win::GetUndocumentedDPITouchScale();

  // Detect and exclude stationary moves.
  if (touch_input.type == TPT_MOVED &&
      touch_point->screenPosition.x == coordinatesX &&
      touch_point->screenPosition.y == coordinatesY &&
      touch_point->radiusX == touch_input.deltaX &&
      touch_point->radiusY == touch_input.deltaY) {
    touch_point->state = WebKit::WebTouchPoint::StateStationary;
    return true;
  }

  touch_point->screenPosition.x = coordinatesX;
  touch_point->screenPosition.y = coordinatesY;
  static float scale = gfx::win::GetDeviceScaleFactor();
  touch_point->position.x = coordinatesX / scale;
  touch_point->position.y = coordinatesY / scale;
  touch_point->radiusX = touch_input.deltaX;
  touch_point->radiusY = touch_input.deltaY;
  touch_point->force = 0;
  touch_point->rotationAngle = 0;
  return false;
}

// Find (or create) a mapping for _os_touch_id_.
unsigned int CefWebTouchState::GetMappedTouch(unsigned int os_touch_id) {
  return id_generator_.GetGeneratedID(os_touch_id);
}
#endif	// defined(OS_WIN)
